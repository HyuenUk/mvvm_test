package com.my.mvvm.recyclerItem;

import android.databinding.ObservableField;

import java.io.Serializable;

public class UserViewModel implements Serializable {
    public final ObservableField<Integer> uid = new ObservableField<>();
    public final ObservableField<String> name = new ObservableField<>();
    public final ObservableField<String> email = new ObservableField<>();

    public UserViewModel(String first, String second) {
        this.name.set(first);
        this.email.set(second);
    }

    public UserViewModel(int _uid, String first, String second) {
        this.uid .set(_uid);
        this.name.set(first);
        this.email.set(second);
    }
}
