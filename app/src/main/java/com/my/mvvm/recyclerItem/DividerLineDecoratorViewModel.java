package com.my.mvvm.recyclerItem;

import android.content.Context;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;

import com.my.mvvm.Base.BaseViewModel;
import com.my.mvvm.R;

public class DividerLineDecoratorViewModel implements BaseViewModel {
    public final ObservableField<DividerItemDecoration> decorator = new ObservableField<>();
//    public final ObservableField<Drawable> drawable = new ObservableField<>();
    public Context context;

    @Override
    public void onCreate() {
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.item_decorator);
        DividerItemDecoration mDecorator = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        mDecorator.setDrawable(drawable);
        decorator.set(mDecorator);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }
}
