package com.my.mvvm.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.my.mvvm.R;
import com.my.mvvm.Base.MyCallActivityNavigator;
import com.my.mvvm.Room.Database.AppDatabase;
import com.my.mvvm.myViewModel.MainViewModel;
import com.my.mvvm.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements MyCallActivityNavigator {
    //    private MainViewModel model = new MainViewModel();
    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setModel(new MainViewModel(this));

        binding.getModel().onCreate();
        // make DB
        AppDatabase.getDatabase(getApplication());

//        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
//        binding.set
//        binding.setModel(model);
//        model.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.getModel().onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        binding.getModel().onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.getModel().onDestroy();
    }

    @Override
    public void CallActivity() {
        startActivity(new Intent(getApplicationContext(), SecActivity.class));
        finish();
    }

    @Override
    public void mHandleActivity() {
        CallActivity();
    }
}

