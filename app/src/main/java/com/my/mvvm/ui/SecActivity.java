package com.my.mvvm.ui;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;

import com.my.mvvm.R;
import com.my.mvvm.Base.MyCallActivityNavigator;
import com.my.mvvm.Room.Entity.User;
import com.my.mvvm.databinding.ActivitySecBinding;
import com.my.mvvm.myViewModel.SecViewModel;
import com.my.mvvm.recyclerItem.DividerLineDecoratorViewModel;
import com.my.mvvm.recyclerItem.UserViewModel;
import com.my.mvvm.recyclerAdapter.SecRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SecActivity extends AppCompatActivity implements MyCallActivityNavigator {
    private SecViewModel secViewModel = null; // = new SecViewModel();
    private DividerLineDecoratorViewModel decoratorViewModel = new DividerLineDecoratorViewModel();
    private SecRecyclerAdapter mAdapter;
    private LifecycleOwner owner;
    private Lifecycle cycle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySecBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_sec);
        secViewModel = new SecViewModel(getApplication());
        binding.setSecModel(secViewModel);
        binding.setDecorator(decoratorViewModel);
        decoratorViewModel.context = getApplicationContext();
        decoratorViewModel.onCreate();
//        observer = new MyObserver();
//        cycle.addObserver(observer);

        mAdapter = new SecRecyclerAdapter(getApplicationContext());
        mAdapter.setHasStableIds(true);
        RecyclerView recyclerView = findViewById(R.id.sec_recyclerView);
//        recyclerView.getRecycledViewPool().setMaxRecycledViews(0,0);
        recyclerView.setAdapter(mAdapter);
        secViewModel.getAllUsers().observe(this, users -> mAdapter.setUserData(users));
//        mAdapter.notifyDataSetChanged();

//        mUser = new User[]{new User(1, "test1", "tt"), new User(2, "asdf", "aa")};
//        mUser = new ArrayList<>();
//        new InsertThread(mUser).start();
//        new SelectAllThread().start();
    }

//    @BindingAdapter({"item"})
//    public static void setUserList(RecyclerView recyclerView, LiveData<List<User>> users) {
//        SecRecyclerAdapter adapter;
//
//        if (recyclerView.getAdapter() == null) {
//            adapter = new SecRecyclerAdapter();
//            recyclerView.setAdapter(adapter);
//        } else {
//            adapter = (SecRecyclerAdapter) recyclerView.getAdapter();
//        }
//
//        adapter.addLiveData(users);
//        if (adapter.getItemCount() != 0)
//            recyclerView.post(() -> recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1));
////        Log.e("items original", users.size() + "");
//    }
//
//    @BindingAdapter({"items", "data"})
//    public static void setUserList(RecyclerView recyclerView, ArrayList<UserViewModel> users, String _data) {
//        SecRecyclerAdapter adapter;
//
//        if (recyclerView.getAdapter() == null) {
//            adapter = new SecRecyclerAdapter();
//            recyclerView.setAdapter(adapter);
//        } else {
//            adapter = (SecRecyclerAdapter) recyclerView.getAdapter();
//        }
////        mUser = new User(users.);
////        adapter.add(users);
//        if (adapter.getItemCount() != 0)
//            recyclerView.post(() -> recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1));
//        Log.e("items add _data", _data + " " + users.size());
//
//    }

    @BindingAdapter({"decorator"})
    public static void setDecorator(RecyclerView recyclerView, ObservableField<DividerItemDecoration> decorator) {
        recyclerView.addItemDecoration(decorator.get());
    }

    @Override
    public void CallActivity() {

    }

    @Override
    public void mHandleActivity() {
        CallActivity();
    }
}
