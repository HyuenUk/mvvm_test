package com.my.mvvm.myViewModel;

import android.app.Application;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.support.annotation.Nullable;
import android.util.Log;

import com.my.mvvm.Base.BaseViewModel;
import com.my.mvvm.Base.MyCallActivityNavigator;
import com.my.mvvm.Base.MyObserver;
import com.my.mvvm.Repository.UserRepository;
import com.my.mvvm.Room.DAO.UserDAO;
import com.my.mvvm.Room.Database.AppDatabase;
import com.my.mvvm.Room.Entity.User;
import com.my.mvvm.recyclerItem.UserViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SecViewModel implements BaseViewModel, MyCallActivityNavigator {
    public final ObservableArrayList<UserViewModel> users = new ObservableArrayList<>();
    public final ObservableField<String> data = new ObservableField<String>();
    private int num = 1;

    //    public List<User> mUser;
    public LiveData<List<User>> mUser;
    private UserRepository mRepo;
    private Application application;

    public SecViewModel(Application application) {
        this.application = application;
        mRepo = new UserRepository(application);
        mUser = mRepo.getAllUser();
//        Log.e("length", mUser.size()+"");
    }

//    public List<User> getAllUsers() {
//        return mUser;
//    }

    public LiveData<List<User>> getAllUsers() {
        return mUser;
    }

    public void insert(User user) {
        Log.e("SecViewModel", "Insert Data");
        mRepo.InsertData(user);
    }

    public void getData() {
        mUser = mRepo.getAllUser();
//        Log.e("length", mUser.size()+"");
//        for(int i = 0;i<mUser.size();i++){
//            Log.e("Data", mUser.get(i).getUid()+", "+mUser.get(i).getName()+" "+mUser.get(i).getNum());
//        }


//        Log.e("length", mUser.getValue().size()+"");
//        for(int i = 0;i<mUser.getValue().size();i++){
//            Log.e("Data", mUser.getValue().get(i).getUid()+", "+mUser.getValue().get(i).getName()+" "+mUser.getValue().get(i).getNum());
//        }
        Log.e("SecViewModel", "getData On");



//        mUser.observe(observer, users -> {
//            Log.e("length", users.size() + "");
//            for (int i = 0; i < users.size(); i++) {
//                Log.e("Data", users.get(i).getUid() + ", " + users.get(i).getName() + " " + users.get(i).getNum());
//            }
//        });
    }

    public void InsertUser() {
//        users.add(new SecViewModel("name "+ String.valueOf(()-> new Random().nextInt()), "email " + ()-> new Random().nextInt()));
//        users.add(new UserViewModel(setUid(), "name " + StrRand(Size()), "num " + IntRand()));
        insert(new User(StrRand(Size()), StrRand(Size())));
    }

    private int Size() {
        return new Random().nextInt(10) + 1;
    }

    private char Char() {
        return (char) (new Random().nextInt(25) + 97);
    }

    private String StrRand(int _size) {
        StringBuilder tmp = new StringBuilder();
        for (int i = 0; i < _size; i++) {
            tmp.append(Char());
        }
        return tmp.toString();
//        for(String arr : SizeRand(_size))
    }


    @Override
    public void onCreate() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void CallActivity() {

    }

    @Override
    public void mHandleActivity() {

    }
//    rand()-> new Random().nextInt();
//    }
//        users.add(new UserViewModel("name " + StrRand(Size()), "num " + IntRand()));
////        users.add(new SecViewModel("name "+ String.valueOf(()-> new Random().nextInt()), "email " + ()-> new Random().nextInt()));
//    public void newUser() {
//
//    private int IntRand() {
//        return new Random().nextInt(100) + 1;
//    }
//
//
//    private int setUid() {
//        return ++num;
//    }
//
//    public void newData() {
//        data.set(StrRand(Size()));
//    }
//
//    public void MakeData() {
//        newUser();
//        newData();
//    }

}
