package com.my.mvvm.myViewModel;

import android.databinding.ObservableField;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.my.mvvm.Base.BaseViewModel;
import com.my.mvvm.Base.MyCallActivityNavigator;

public class MainViewModel implements BaseViewModel, MyCallActivityNavigator {
    private MyCallActivityNavigator navigator;
    public final ObservableField<String> HelloTxt = new ObservableField<>();

    public MainViewModel(MyCallActivityNavigator navigator){
        this.navigator = navigator;
    }

    public void onCreate() {
        HelloTxt.set("Hello! Test MVVM!");
    }

    public void showCurrentTime() {
        HelloTxt.set(String.valueOf(System.currentTimeMillis()));
    }

    public View.OnClickListener currentTime2ClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//                showCurrentTime();
            HelloTxt.set("My Test!");
//            Intent intent = new Intent(, SecActivity.class);
        }
    };

    @Override
    public void onResume() {
        Log.e("MainViewModel", "onCreate()");
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void CallActivity(){
        navigator.CallActivity();
    }

    @Override
    public void mHandleActivity() {
        HelloTxt.set("Bye Bye MainActivity~~~");

        // JDK 1.8 Handler lambda
        Handler handler = new Handler();
        handler.postDelayed(this::CallActivity, 500);
    }
}
