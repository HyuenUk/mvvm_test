package com.my.mvvm.myViewModel;

import android.databinding.ObservableField;
import android.util.Log;

import com.my.mvvm.Base.BaseViewModel;
import com.my.mvvm.Base.MyCallActivityNavigator;
import com.my.mvvm.recyclerItem.UserViewModel;


public class FragmentViewModel implements BaseViewModel, MyCallActivityNavigator {
    private MyCallActivityNavigator navigator;
    private UserViewModel data = null;
    public final ObservableField<String> name = new ObservableField<>();
    public final ObservableField<String> email = new ObservableField<>();

    public FragmentViewModel(MyCallActivityNavigator navigator, UserViewModel _data){
        this.navigator = navigator;
        this.data = _data;
        Log.e("New", "Bundle");
    }

    @Override
    public void onCreate() {
        showTxt();

    }

    @Override
    public void onResume() {

    }

    private void showTxt() {
        Log.e("FragData", "name : "+ data.name +" / email : " + data.email);
        Log.e("FragData", "name : "+ data.name.get() +" / email : " + data.email.get());

        name.set(String.valueOf(data.name.get()));
        email.set(String.valueOf(data.email.get()));
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void CallActivity() {
        Log.e("Frag", "OnClick");
        navigator.CallActivity();
    }

    @Override
    public void mHandleActivity() {

    }
}
