package com.my.mvvm.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.my.mvvm.Room.DAO.UserDAO;
import com.my.mvvm.Room.Database.AppDatabase;
import com.my.mvvm.Room.Entity.User;

import java.util.List;

public class UserRepository {
    private UserDAO dao;
    //    List<User> mLiveData;
    LiveData<List<User>> mLiveData;

    public UserRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        Log.e("repo", "getAll List");
        dao = db.userDAO();
        mLiveData = dao.getAllList();
    }

//    public List<User> getAllUser(){
//        return mLiveData;
//    }

    public LiveData<List<User>> getAllUser() {
        mLiveData = dao.getAllList();
        return mLiveData;
    }

    public void InsertData(User user) {
        new InsertAsyncTask(dao).execute(user);
    }

    private static class InsertAsyncTask extends AsyncTask<User, Void, Void> {
        private UserDAO mDao = null;

        private InsertAsyncTask(UserDAO _dao) {
            this.mDao = _dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            Log.e("Repo insert", params[0].getName() + " " + params[0].getNum());
            mDao.insert(params[0]);
            return null;
        }
    }
}
