package com.my.mvvm.recyclerAdapter;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.my.mvvm.Base.BaseRecyclerViewHolder;
import com.my.mvvm.R;
import com.my.mvvm.Room.Entity.User;
import com.my.mvvm.ViewHolder.SecRecyclerViewHolder;

import java.util.List;

public class SecRecyclerAdapter extends RecyclerView.Adapter<SecRecyclerViewHolder> {
    private Context context;
    private List<User> mList;

    public SecRecyclerAdapter(Context context) {
        this.context = context;
    }
    //    private ArrayList<UserViewModel> users = new ArrayList<>();


//    public void add(ArrayList<UserViewModel> users) {
//        for (UserViewModel user : users) {
//            if (!this.users.contains(user)) {
//                this.users.add(user);
//                notifyItemInserted(this.users.size() - 1);
//            }
//        }
//    }

    public void addListData(List<User> mUser) {

    }

    public void addLiveData(LiveData<List<User>> mUser) {

    }

    private void remove(int position) {
        if (mList != null) {
            mList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, mList.size());
        } else
            Log.e("Adapter", "null");
    }

    @NonNull
    @Override
    public SecRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        return new SecRecyclerViewHolder(inflater.inflate(R.layout.sec_item_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SecRecyclerViewHolder holder, int i) {
        if (mList != null) {
            User mData = mList.get(i);
//            Log.e("AdapterData", mData.getUid() + " / " + mData.getName() + " / " + mData.getNum());
//          holder.binding().setUser(users.get(i)); // ObservableField
//            holder.binding().setUser();

//            holder.itemView.findViewById(R.id.sec_item_first)
//            holder.binding().secItemFirst.setText(mData.getName());
//            holder.binding().secItemSecond.setText(mData.getNum());
            holder.tv1.setText(mData.getName());
            holder.tv2.setText(mData.getNum());

            holder.itemView.setOnClickListener(view -> OnClickMyItem(view, i, mData, mData.getUid()));
            holder.itemView.setTag(i);
            holder.itemView.setOnLongClickListener(view -> OnLongClickMyItem(view, i));
//        holder.binding().setNavigator(navigator);
        } else
            Log.e("onBind", i+" null");
    }

    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        else
            return 0;
    }

    private void OnClickMyItem(View view, int position, User mData, int uid) {
        Toast.makeText(view.getContext(), "Position, " + position + " / data" + mData.getUid() + " / uid" + uid, Toast.LENGTH_SHORT).show();
    }


    private boolean OnLongClickMyItem(View view, int position) {
        Log.e("Long Position", position + "!");
        Toast.makeText(view.getContext(), "Long Position, " + position, Toast.LENGTH_SHORT).show();
        remove(position);
        return true;
    }

    public void setUserData(List<User> users) {
        mList = users;
        notifyDataSetChanged();
    }


//    private void showFragment(UserViewModel model) {
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("data", model);
//        Log.e("Data", model.name.get() + " / " + model.email.get());
//        FragmentPage page = FragmentPage.getInstance();
//        page.setArguments(bundle);
//        FragmentManager fragmentManager = ((SecActivity) context).getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.sec_frag, page).commit();
//    }
}
