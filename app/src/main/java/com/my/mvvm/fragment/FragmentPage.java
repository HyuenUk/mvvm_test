package com.my.mvvm.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.my.mvvm.Base.MyCallActivityNavigator;
import com.my.mvvm.R;
import com.my.mvvm.databinding.FragmentAdapterItemBinding;
import com.my.mvvm.myViewModel.FragmentViewModel;
import com.my.mvvm.recyclerItem.UserViewModel;

public class FragmentPage extends Fragment implements MyCallActivityNavigator {
    private static FragmentPage mPage = null;

    public FragmentPage() {

    }

    public static FragmentPage getInstance(){
        if(mPage == null)
            mPage = new FragmentPage();
        return mPage;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return DataBindingUtil.inflate(inflater, R.layout.fragment_adapter_item, container, false).getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        FragmentViewModel model = new FragmentViewModel(this, (UserViewModel)bundle.getSerializable("data"));
        FragmentAdapterItemBinding binding = DataBindingUtil.getBinding(getView());
        binding.setFragModel(model);
        model.onCreate();
    }

    @Override
    public void CallActivity() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().remove(FragmentPage.this).commit();
        fragmentManager.popBackStack();
        Toast.makeText(getContext(), "Fragment Close", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void mHandleActivity() {

    }
}
