package com.my.mvvm;

import android.databinding.ObservableField;
import android.util.Log;
import android.view.View;

import com.my.mvvm.Base.BaseViewModel;

public class DataViewModel implements BaseViewModel {
    public final ObservableField<String> HelloTxt = new ObservableField<>();

    public void onCreate() {
        HelloTxt.set("Hello! Test MVVM!");
    }

    public void showCurrentTime() {
        HelloTxt.set(String.valueOf(System.currentTimeMillis()));
    }

    public View.OnClickListener currentTime2ClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//                showCurrentTime();
            HelloTxt.set("My Test!");
//            Intent intent = new Intent(, SecActivity.class);
        }
    };

    @Override
    public void onResume() {
        Log.e("DataViewModel", "onCreate()");
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }
}

