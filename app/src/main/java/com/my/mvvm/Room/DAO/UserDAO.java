package com.my.mvvm.Room.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.my.mvvm.Room.Entity.User;
import com.my.mvvm.recyclerItem.UserViewModel;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface UserDAO {
//    @Query("select * from user")
//    MutableLiveData<ArrayList<UserViewModel>> getAllArrayList();

    @Query("select * from User")
//    List<User> getAllList();
    LiveData<List<User>> getAllList();

    @Insert
    void insertAll(User[] users);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User user);
}
