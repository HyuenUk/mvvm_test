package com.my.mvvm.Room.Database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.my.mvvm.Room.ASync.PopulateDBAsync;
import com.my.mvvm.Room.DAO.UserDAO;
import com.my.mvvm.Room.Entity.User;

@Database(entities = {User.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDAO userDAO();

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Context context) {
        Log.e("AppDatabase", "make DB");
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    Log.e("AppDatabase", "new DB");
                    INSTANCE = Room.databaseBuilder(context, AppDatabase.class, "myData.db")
//                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {
                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    Log.e("PopulateDB", "Open DB");
                    new PopulateDBAsync(INSTANCE).execute();
                }
            };
}

