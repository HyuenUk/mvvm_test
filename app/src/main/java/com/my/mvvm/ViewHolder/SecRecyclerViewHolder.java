package com.my.mvvm.ViewHolder;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.my.mvvm.R;

public class SecRecyclerViewHolder extends RecyclerView.ViewHolder {
//    private final SecItemLayoutBinding binding;
    public TextView tv1, tv2;

    public SecRecyclerViewHolder(@NonNull View v) {
        super(v);
//        this.binding = DataBindingUtil.setContentView((Activity) v.getParent().getParent(), R.layout.sec_item_layout);

//        this.binding() = DataBindingUtil.bind(v);
        tv1 = v.findViewById(R.id.sec_item_first);
        tv2 = v.findViewById(R.id.sec_item_second);
    }

//    public SecItemLayoutBinding binding(){
//        return binding;
//    }
}
