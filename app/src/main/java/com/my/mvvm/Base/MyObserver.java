package com.my.mvvm.Base;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

public class MyObserver implements LifecycleObserver, LifecycleOwner {
    private String TAG = "[Observer]";
    private LifecycleRegistry registry = new LifecycleRegistry(this);

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return registry;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    public void onStateEvent(LifecycleOwner owner, Lifecycle.Event event){
        registry.handleLifecycleEvent(event);
    }
}
