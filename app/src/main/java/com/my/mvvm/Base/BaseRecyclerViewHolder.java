package com.my.mvvm.Base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

public class BaseRecyclerViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
    private final T binding;

    public BaseRecyclerViewHolder(View view) {
        super(view);
        this.binding = DataBindingUtil.bind(view);
        // 1.8 lambda OnClickListener
//        view.setOnClickListener(v -> Log.e("BaseRecyclerViewHolder", "OnClick Test, Position : "+(Integer) v.getTag()));
    }

    public T binding() {
        return binding;
    }
}
