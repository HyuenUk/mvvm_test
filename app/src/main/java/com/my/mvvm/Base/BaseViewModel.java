package com.my.mvvm.Base;


public interface BaseViewModel {
    void onCreate();
    void onResume();
    void onDestroy();
    void onPause();
}
